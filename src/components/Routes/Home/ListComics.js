import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import Table from '../../Table'

const styles = {
  card: {
    maxWidth: 345
  },
  media: {
    objectFit: 'cover',
    width: 180,
    height: 213
  }
}

const ListComics = ({ classes }) => (
  <Query
    query={gql`
      {
        comicsQuery(q: "", pageSize: 4) {
          id
          image
          name
          otherName
          slug
          authorId
          artistId
          releasedYear,
          createdAt
        } 
      }
    `}
  >
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>
      if (error) return
      const tableProps = {
        dataSource: data.comicsQuery,
        columns: [
          {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: (text, record) => <Link href={`/list/${record.slug}`}>{record.name}</Link>
          },
          {
            title: 'Chapter',
            dataIndex: 'chapter',
            key: 'chapter'
          },
          {
            title: 'CreatedAt',
            dataIndex: 'createdAt',
            key: 'createdAt'
          }
        ]
      }
      return (
        <div>
          <Grid container className={classes.root} spacing={16}>
            <Grid item xs={12}>
              <Grid container className={classes.demo} justify="left" spacing={32}>
                {data.comicsQuery.map(({
                  id,
                  image,
                  name,
                  slug
                }) => {
                  return (
                    (
                      <Grid key={id} item>
                        <Link href={`list/${slug}`}>
                          <Card to={slug} key={id} className={classes.card}>
                            <CardActionArea>
                              <CardMedia
                                component="img"
                                alt="Image"
                                className={classes.media}
                                image={image}
                                title={name}
                              />
                              <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                  {name}
                                </Typography>
                                <Typography component="p">
                                  Lizards are ranging
                                </Typography>
                              </CardContent>
                            </CardActionArea>
                          </Card>
                        </Link>
                      </Grid>
                    )
                  )
                })}
              </Grid>
            </Grid>
          </Grid>
          <Table {...tableProps} />
        </div>
      )
    }}
  </Query>
)

ListComics.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(ListComics)
